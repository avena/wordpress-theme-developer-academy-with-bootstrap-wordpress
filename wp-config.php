<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tranquil-spa');

/** MySQL database username */
define('DB_USER', 'spa-admin');

/** MySQL database password */
define('DB_PASSWORD', '1ev1xvQO3S2eIHtD');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'I5g. 9r$MCVzQ0#So4sKG7(1N]mLiSUqj/|PO)e.uv#fmTc+Dt5Q#U-]k;LpN|*c');
define('SECURE_AUTH_KEY',  'mq!|3e$=SAp`<SC/L-(y&a)AJ?+jB%TOunCH8JEM+e6O-0z#R|4lG~)DNi^zL_J(');
define('LOGGED_IN_KEY',    'C%cXydg|7Ug,r75?@_?*j;%xjR^kd V?T&|rN9;vLl=j),bc81Q9A?2`>J^&Ap`J');
define('NONCE_KEY',        'ka^Py}p-hPS|b+d{AQ66[w(uIU;WSo?| vV&KPUZfZt~)mJ:eVT!da$0yV)?O,;g');
define('AUTH_SALT',        ' |/(2>cvT~m~nj?3y_F?{%D1FL`ay)45tGksv:QSB>Kv@31x$;dm:Qa&?j6f8G4M');
define('SECURE_AUTH_SALT', ';[10R-JHB]~|G]s/>vb)x2@-,J?&GmIwr-AW1l/ZE 9CMh.L&a8SPI~e}a|yO+)D');
define('LOGGED_IN_SALT',   '$YVNBd/!_xXj)PfF+%oe!a69gDY|A:hmw(AL8%EAOBUSqTjeWN}F;*duX)RJj64u');
define('NONCE_SALT',       'CF}~A3]|e4urOiC=z )s.^sfW}!#:IY!`J*Gzyw{;3B@O]N[(XK Xk|{{UV(:Wh4');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'tranquildb_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
//define('WP_DEBUG', false);
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
