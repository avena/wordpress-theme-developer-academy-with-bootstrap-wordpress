<?php
/**
* Template Name: About Us
*/

get_header(); ?>

<!-- about section-->
<section id="about">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <h2><?php _e( 'About Us', 'tranquilwp');?></h2>
                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/divider-purple.png" alt="divider">
            </div> <!-- col -->
        </div> <!-- row -->

        <div class="row">
            <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam quis pharetra nisl. Quisque ultricies nisl a fermentum auctor. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Phasellus id sem lorem. Vestibulum non mi augue. Vivamus nec velit eu ipsum euismod lobortis quis ac nisl. Morbi leo justo, laoreet ac consectetur sit amet, posuere ac erat.
            </p>
            <p>
            Aliquam dignissim, risus sit amet ornare mattis, dolor arcu dignissim ligula, viverra condimentum sem purus sed elit. Cras ac iaculis nisl, a aliquet mauris. Etiam fermentum urna nunc, vel sollicitudin libero convallis et. Donec volutpat orci eu nisl dignissim pellentesque. Duis lacus odio, rutrum nec pellentesque sit amet, finibus at velit. Proin iaculis leo eget tristique tempor. Aenean facilisis nisl quis erat vestibulum, ac auctor mauris ultricies. Duis rhoncus egestas ex, dapibus consequat odio auctor nec.
            </p>
        </div>

        <?php get_template_part( 'content', 'facilities' ); ?>

    </div> <!-- container -->
</section>



<?php get_footer(); ?>
