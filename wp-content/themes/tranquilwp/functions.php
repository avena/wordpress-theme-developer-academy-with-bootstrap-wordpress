<?php

    if(! isset($content_width)) {
        $content_width = 660;
    }

    function tranquilwp_setup() {

        // configurando que o tema tranquilwp -  nome definido no style.css
        // vai carregar os template de linguagem em /languages
        load_theme_textdomain('tranquilwp', get_template_directory() . '/languages');

        // Enabling Support for Post Thumbnails
        add_theme_support( 'post-thumbnails' );

        add_theme_support('automatic-fee-links');
        add_theme_support('title-tag');
        // Register Custom Navigation Walker
        require_once get_template_directory() . '/wp-bootstrap-navwalker.php';
        register_nav_menus( array(
            'primary' => __( 'Primary Menu', 'tranquilwp' ),
            'footer' => __( 'Footer Menu', 'tranquilwp' )
        ) );
    }
    add_action('after_setup_theme','tranquilwp_setup');


    function tranquil_scripts() {

        /* add styles */
        wp_enqueue_style('bootstrap-core', get_template_directory_uri() . '/css/bootstrap.min.css');
        wp_enqueue_style('custom', get_template_directory_uri() . '/style.css');

        /* add scripts */
        //        no curso o bootstrap.min.js ficava com valor de versao na frente do nome
        //        e nao funcionava corretamente.
        //        wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), true);
        //        solução usada do projeto b4st - A Bootstrap 4 Starter Theme, for WordPress
        //        https://github.com/SimonPadbury/b4st/blob/master/functions/enqueues.php
        wp_register_script('jquery-1.12.4', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js', false, '1.12.4', true);
		wp_enqueue_script('jquery-1.12.4');
        //        wp_register_script('bootstrap-js', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js', false, '3.3.5', true);
        //        wp_enqueue_script('bootstrap-js');
        // usando bootstrap.min.js local
        wp_register_script('bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', false, '3.3.5', true);
		wp_enqueue_script('bootstrap-js');
        //        script comment-reply quando foi pagina single,
        //        assim caixa de comentario aparece logo abaixo da outro comentario
         if(is_singular() ) wp_enqueue_script('comment-reply');
    }




    add_action('wp_enqueue_scripts','tranquil_scripts');

		function custom_excerpt_more( $more ) {
			return '...';
		}
		add_filter( 'excerpt_more', 'custom_excerpt_more' );

    /**
     * Register our sidebars and widgetized areas.
     *
     */
    function arphabet_widgets_init() {

      register_sidebar( array(
        'name'          => 'Sidebar',
        'id'            => 'sidebar_blog',
        'before_widget' => '<div class="sidebar-module">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>',
      ) );

    }
    add_action( 'widgets_init', 'arphabet_widgets_init' );

		function featureText() {
			if(is_front_page()) {
    //     usando o campo criado no Advanced Custom Fields
				the_field('feature_text');

			} elseif( is_home() || is_single() ) {
				_e('Tranquil Spa Official Blog', 'tranquilwp');
			} elseif(is_archive() ) {
        //_e  = echo
        _e('Tranquil Spa Official Blog', 'tranquilwp');
        _e('<br>');
        // pega o term da categoria, arquivo da categoria
        single_term_title(__('Browsing: ', 'tranquilwp'));
        // se o arquivo for do mes e ano, vai pegar a data e converter para o nome do mes
        if(is_month()) {
            $monthNum = get_query_var('monthnum');
            $month = date("F",mktime(0, 0, 0, $monthNum));
            $year = get_query_var('year');

            echo 'Post from ' . $month . ' ' . $year;

        }
        // verifica se usa o template page-news
      } elseif(is_page_template('page-news.php') || is_page_template('page-about.php') || is_page_template('page-contact.php')) {
          bloginfo('name');
          _e('<br>');
          the_title();
      } elseif ( is_404() ) {
          _e('Whoops, were a little lost', 'tranquilwp');
      } elseif ( is_search() ) {
          _e('Tranquil Spa Official Blog', 'tranquilwp');
          _e('<br>');

          printf( __('Search results for: %s', 'tranquilwp'), get_search_query() );
      }

		}




$args = array(
	//'width'         => 980,
	//'height'        => 60,
    'default-image' => get_template_directory_uri() . '/images/lakebgred.jpg',
    'uploads' => true,

);
add_theme_support( 'custom-header', $args );




?>
